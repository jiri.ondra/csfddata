import unicodedata
import requests
from bs4 import BeautifulSoup
import sqlite3
import traceback
import os


BASE_URL = "https://www.csfd.cz"


db_con = sqlite3.connect('csfd.db')
db_cur = db_con.cursor()

movie_a = [] # movie list - movie_id, name, rank, search_string
actor_a = [] # actor list - actor_id, fullname, search_string
movie_actor_a = [] # movie actor pairs

def remove_accents(input_str):
    '''remove accents and filter ASCII'''
    upd = unicodedata.normalize('NFKD', input_str).encode('ASCII', 'ignore').decode("utf-8")
    return upd.lower()

def get_numbers(str_in: str):
    return int(''.join(filter(lambda x: x.isdigit(), str_in)))

def get_parsed(s: str):
    return BeautifulSoup(s, features="html.parser")

if __name__ == "__main__":

    with requests.get(f"{BASE_URL}/zebricky/nejlepsi-filmy/?show=complete", timeout=10) as r:
        
        parsed_html = get_parsed(r.text)


        movie_list = parsed_html.find("table", {"class":"content ui-table-list striped"})\
            .find_all("tr")

    for m in movie_list:
        class_film = m.find("td", {"class": "film"})
        class_rank = m.find("td", {"class": "order"})

        try:
            movie_id = get_numbers(class_film["id"])
            print(class_rank)
        except:
            continue

        movie_row = [
            movie_id,
            class_film.find('a').text,
            get_numbers(class_film.find('span').text),
            get_numbers(class_rank.text),
            remove_accents(class_film.find('a').text)

        ]

        # print(movie_row)
        # os._exit(0)

        db_cur.execute('INSERT OR IGNORE INTO movie VALUES (?,?,?,?,?)', movie_row)
        db_con.commit()
        # print(movie_a)

        # get actors for current movie
        # only visible actors on page - to get all use ie. selenium instead of requests lib
        with requests.get(f"{BASE_URL}/film/{movie_id}") as r:
            # print(r.text)

            
            parsed_html = get_parsed(r.text)
            actor_list = []
            for div_ in parsed_html.find("div", {"class":"creators"}).find_all("div"):
                if "Hrají" in div_.find("h4").text:
                    actor_list = div_.find_all("a")

            for a in actor_list:
                actor_id = get_numbers(a["href"])
                actor_a.append([
                    actor_id,
                    a.text,
                    remove_accents(a.text)
                ])

                movie_actor_a.append([movie_id, actor_id])

            db_cur.executemany('INSERT OR IGNORE INTO actor VALUES (?,?,?)', actor_a)
            db_con.commit()

            db_cur.executemany('INSERT OR IGNORE INTO movie_actor VALUES (?,?)', movie_actor_a)
            db_con.commit()




from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy

from prepare_data import remove_accents

import os
import traceback
import sqlite3
import unicodedata


app = Flask(__name__)
base_dir = os.path.abspath(os.path.dirname(__file__))
db_file = "csfd.v1.db"
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///{}".format(os.path.join(base_dir, db_file))
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)


movie_actor = db.Table('movie_actor',
    db.Column('movie_id', db.Integer, db.ForeignKey('movie.movie_id'), primary_key=True),
    db.Column('actor_id', db.Integer, db.ForeignKey('actor.actor_id'), primary_key=True)
)

class Movie(db.Model):
    __tablename__ = 'movie'
    movie_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    year = db.Column(db.Text)
    rank = db.Column(db.Integer)
    search_string = db.Column(db.Text)

    actors = db.relationship('Actor', secondary=movie_actor, lazy='subquery',
        backref=db.backref('movie', lazy=True))            

class Actor(db.Model):
    __tablename__ = 'actor'
    actor_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    search_string = db.Column(db.Text)
    movies = db.relationship('Movie', secondary=movie_actor, lazy='subquery',
        backref=db.backref('actor', lazy=True))                

@app.route('/index')
def get_index():
    return render_template('index.html')

@app.route('/get_data', methods = ['POST', 'GET'])
def get_data():
    """Return dict with movies and actors by search string
    """
    movies = []
    actors = []
    qstring = request.form.to_dict().get('search_string')
    if qstring:
        qstring = remove_accents(qstring)
        movies = Movie.query.filter(Movie.search_string.like(f'%{qstring}%')).order_by("name").all()
        actors = Actor.query.filter(Actor.search_string.like(f'%{qstring}%')).order_by("name").all()

    return render_template('index.html', movies=movies, actors=actors )

@app.route('/get_detail/<detail_type>/<item_id>', methods = ['GET'])
def get_detail(detail_type, item_id):
    """Return detail of movie or actor by its type and id
    """

    title = ""
    subtitle = ""
    rows = []
    row_type = ""
    try:
        if detail_type == "movie":
            qresult = Movie.query.filter_by(movie_id=item_id).first()
            title = qresult.name
            rows =  qresult.actors
            row_type = "actor"
            subtitle = "Herci"
        else:
            qresult = Actor.query.filter_by(actor_id=item_id).first()
            title = qresult.name
            rows =  qresult.movies
            row_type = "movie"
            subtitle = "Filmy"
    except:
        print(traceback.format_exc())

    return render_template('detail.html', title=title, subtitle=subtitle, rows=rows, row_type=row_type)


if __name__ == '__main__':
   app.run(host='0.0.0.0', port=5002, debug=False)

